#/bin/bash

function db_setup() {
  mysql  -u $username -p$password < sql/DDL/CreateDB.sql
  mysql  -u $username -p$password < sql/DDL/CreateTable.sql
  mysql  -u $username -p$password < sql/DDL/CreateAvgTable.sql
}

while [[ -n "$1" ]]; do

  case $1 in
    -db|--dbsetup)
      db_setup
      shift
      ;;

    -dd|--data-dir)
      if [ -n "$2" ] && [ "${2:0:1}" != "-" ]; then
        data_dir=$2
        shift 2
      else
        echo "Miss one argument $1"
        exit 1
      fi
      ;;

    -pm|--processing-method)
      if [ -n "$2" ] && [ "${2:0:1}" != "-" ]; then
        processing_method=$2
        shift 2
      else
        echo "Miss one argument $1"
        exit 1
      fi
      ;;
    -dt|--delimiter)
      if [ -n "$2" ] && [ "${2:0:1}" != "-" ]; then
        delimiter=$2
          shift 2
        else
          echo "Miss one argument $1"
          exit 1
        fi
        ;;

    *)
      echo "Error $1"
      exit 1
  esac
done

if [ -n "$data_dir" ] && [ -n "$processing_method" ] && [ -n "$delimiter" ]; then
 scalac  -cp ./lib/config-1.4.1.jar:./lib/mysql-connector-java-8.0.26.jar src/com/company/App.scala
 scala  -cp ./lib/config-1.4.1.jar -cp ./lib/mysql-connector-java-8.0.26.jar com.company.App

else
  echo "Miss one or more arguments $1"
  exit 1
fi