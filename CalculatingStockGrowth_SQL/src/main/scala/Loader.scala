import com.typesafe.config.ConfigFactory
import scala.util.matching.Regex
import java.io.File
import java.sql.{Connection, DriverManager}
import scala.collection.mutable.ArrayBuffer

case class MonthData(NumberOfMonth: Int, OpenPrice: Float, ClosePrice: Float)
case class YearData(NumberOfYear: Int, StockName: String,bid: String, months: ArrayBuffer[MonthData])


object Loader {

  def getDataFromFiles(pathToDataDir: String, delimiter: String): Array[YearData] = {
    val files = new File(pathToDataDir)
      .listFiles.map(_.toString)
      .filter(_.endsWith(".csv"))

    for (file <- files) yield ReadFiles(file, delimiter)
  }

  def ReadFiles(file: String, delimiter: String): YearData = {
    val bufferedSource = io.Source.fromFile(file)

    val FirstLine = bufferedSource
      .getLines.drop(1)
      .next.split(delimiter)

    val CurrentYear: Int = FirstLine(0).slice(0, 4).toInt

    var OpenPrice: String = FirstLine(1).replace(',', '.')
    var ClosePrice: String = ""

    var monthOfRow, currentMoth = 1

    val monthMetricsList = ArrayBuffer[MonthData]()
    for (line <- bufferedSource.getLines) {
      val cols = line.split(";")
      val Date = cols(0)
      currentMoth = Date.slice(5, 7).toInt

      if (currentMoth == monthOfRow) {
        ClosePrice = cols(4).replace(',', '.')
      } else {
        monthMetricsList += MonthData(monthOfRow, OpenPrice.toFloat, ClosePrice.toFloat)
        monthOfRow = monthOfRow + 1
        OpenPrice = cols(1).replace(',', '.')
      }
    }
    bufferedSource.close

    monthMetricsList += MonthData(monthOfRow, OpenPrice.toFloat, ClosePrice.toFloat)

    val StockName = file.split("_")(0).substring(file.lastIndexOf("""\""") + 1)
    val selectPriceValue= raw"(?<=Secs_)(\w{3})"r
    val PriceValue = selectPriceValue.findFirstIn(file).mkString

    val yearMetrics: YearData = YearData(CurrentYear, StockName, PriceValue, monthMetricsList)
    yearMetrics
  }

  def getQueryFromFile(pathToSqlQuery: String): String = {
    val source = io.Source.fromFile(pathToSqlQuery)
    val sqlQuery = try source.mkString finally source.close()
    sqlQuery
  }

  def main(args: Array[String]): Unit = {

    val (pathToDataDir, processingMethod, delimiter) = getArguments(args)

    val yearMetricsList: Array[YearData] = getDataFromFiles(pathToDataDir, delimiter)

    val connection = getConnection
    LoadDataStagingDB(connection, yearMetricsList)

    CalculateGrowthStock(connection, processingMethod)

  }

  def CalculateGrowthStock(connection: Connection, processingMethod: String): Unit = {
    val conf = ConfigFactory.load()

    var sqlQueryCalculatorGrowth = ""
    var sqlQueryCalculatorAverage = ""

    processingMethod match {
      case "o-c" =>
        sqlQueryCalculatorGrowth = getQueryFromFile(conf.getString("pathsToQueries.pathCalculatorGrowth_O_C"))
        sqlQueryCalculatorAverage = getQueryFromFile(conf.getString("pathsToQueries.pathCalculatorAverage_O_C"))

      case "c-o" =>
        sqlQueryCalculatorGrowth = getQueryFromFile(conf.getString("pathsToQueries.pathCalculatorGrowth_C_O"))
        sqlQueryCalculatorAverage = getQueryFromFile(conf.getString("pathsToQueries.pathCalculatorAverage_C_O"))
    }
    val statement = connection.createStatement()
    statement.executeUpdate(sqlQueryCalculatorGrowth)
    statement.executeUpdate(sqlQueryCalculatorAverage)
  }

  def LoadDataStagingDB(connection: Connection, yearMetricsList: Array[YearData]): Unit = {

    val conf = ConfigFactory.load()
    val sqlQueryLoadIntoStaging = getQueryFromFile(conf.getString("pathsToQueries.pathLoadIntoStaging"))
    val preparedStatementLoad = connection.prepareStatement(sqlQueryLoadIntoStaging)

    for (yearMetrics <- yearMetricsList) {
      for (monthMetrics <- yearMetrics.months){
        preparedStatementLoad.setString(1, yearMetrics.StockName)
        preparedStatementLoad.setString(2, yearMetrics.bid)
        preparedStatementLoad.setInt(3, yearMetrics.NumberOfYear)
        preparedStatementLoad.setInt(4, monthMetrics.NumberOfMonth)
        preparedStatementLoad.setFloat(5, monthMetrics.OpenPrice)
        preparedStatementLoad.setFloat(6, monthMetrics.ClosePrice)

        preparedStatementLoad.executeUpdate()
      }
    }
  }

  def getConnection: Connection = {
    val conf = ConfigFactory.load()

    val url = conf.getString("db.url")
    val username = conf.getString("db.user")
    val password = conf.getString("db.password")

    try {
      Class.forName("com.mysql.cj.jdbc.Driver")
      DriverManager.getConnection(url, username, password)
    } catch {
      case e: Exception => throw new InterruptedException(s"Error: ${e.getMessage}")
    }
  }

  def getArguments(args: Array[String]): (String, String, String)= {
    val help: String = """
                         |Using:
                         |-dd, --data-dir <pathToDataDir>
                         |-pm, --processingMethod <processingMethod>
                         |-dt, --delimiter <delimiter>
                         |""".stripMargin

    if (args.length < 6) {
      throw new IllegalArgumentException(help)
    } else {

      var pathToDataDir = ""
      var processingMethod = ""
      var delimiter = ""

      args.sliding(2, 2).toList.collect {
        case Array(option: String, argPath: String) if option == "-dd" || option == "--data-dir" =>  pathToDataDir = argPath
        case Array(option: String, argMethod: String) if option == "-pm" || option == "--processingMethod" => processingMethod = argMethod
        case Array(option: String, argDelimiter: String) if option == "-dt" || option == "--delimiter" => delimiter = argDelimiter
        case _ => throw new IllegalArgumentException("Unknown option\n" + help)
      }
      (pathToDataDir, processingMethod, delimiter)
    }
  }
}
