USE stockmarketdestination;

DROP TABLE IF EXISTS average_months_O_C;
CREATE TABLE average_months_O_C(
    stockName VARCHAR(25) NOT NULL,
	priceValue VARCHAR(3),
    avg_jan DECIMAL(10, 4),
    avg_feb DECIMAL(10, 4),
    avg_mar DECIMAL(10, 4),
    avg_apr DECIMAL(10, 4),
    avg_may DECIMAL(10, 4),
    avg_june DECIMAL(10, 4),
    avg_july DECIMAL(10, 4),
    avg_aug DECIMAL(10, 4),
    avg_sep DECIMAL(10, 4),
    avg_oct DECIMAL(10, 4),
    avg_nov DECIMAL(10, 4),
    avg_dec DECIMAL(10, 4),
    avg_total DECIMAL(10, 4)
);

