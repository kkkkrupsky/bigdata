USE StockMarketDestination;

DROP TABLE IF EXISTS Table_C_С;
CREATE TABLE Table_C_С (
    stockName VARCHAR(25) NOT NULL,
    priceValue VARCHAR(3),
    year SMALLINT NOT NULL,
    january DECIMAL(10, 4),
    february DECIMAL(10, 4),
    march DECIMAL(10, 4),
    april DECIMAL(10, 4),
    may DECIMAL(10, 4),
    june DECIMAL(10, 4),
    july DECIMAL(10, 4),
    august DECIMAL(10, 4),
    september DECIMAL(10, 4),
    october DECIMAL(10, 4),
    november DECIMAL(10, 4),
    december DECIMAL(10, 4),
    total DECIMAL(10, 4)
);