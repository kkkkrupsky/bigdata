USE StockMarketStaging;

DROP TABLE IF EXISTS TableStaging;
CREATE TABLE TableStaging (
    stockName VARCHAR(25) NOT NULL,
	priceValue VARCHAR(3),
    year SMALLINT NOT NULL,
    month SMALLINT NOT NULL,
    OpenPrice DECIMAL(8,4),
    ClosePrice DECIMAL(8,4),
    PRIMARY KEY (stockName, year, month)
);