INSERT INTO stockmarketdestination.table_c_с (stockName,priceValue, year, january, february, march, april, may, june, july, august, september, october, november, december, total)
WITH
add_prev_column as
	(
		select
			stockName,
			priceValue,
			year,
			month,
			OpenPrice,
			ClosePrice,
            LAG(ClosePrice, 1, OpenPrice) OVER(ORDER BY stockName) as prev_ClosePrice
		from StockMarketStaging.TableStaging tb
	),
    calculate_growth as (
		select *,
        ((ClosePrice - prev_ClosePrice) / prev_ClosePrice * 100 ) as growth
        from add_prev_column
    ),
    set_month_growth as (
    select
			stockName,
            priceValue,
    		`year`,
    		MAX(CASE WHEN (`month` = 1) THEN growth ELSE NULL END) AS january,
    		MAX(CASE WHEN (`month` = 2) THEN growth ELSE NULL END) AS february,
    		MAX(CASE WHEN (`month` = 3) THEN growth ELSE NULL END) AS march,
    		MAX(CASE WHEN (`month` = 4) THEN growth ELSE NULL END) AS april,
    		MAX(CASE WHEN (`month` = 5) THEN growth ELSE NULL END) AS may,
    		MAX(CASE WHEN (`month` = 6) THEN growth ELSE NULL END) AS june,
    		MAX(CASE WHEN (`month` = 7) THEN growth ELSE NULL END) AS july,
    		MAX(CASE WHEN (`month` = 8) THEN growth ELSE NULL END) AS august,
    		MAX(CASE WHEN (`month` = 9) THEN growth ELSE NULL END) AS september,
    		MAX(CASE WHEN (`month` = 10) THEN growth ELSE NULL END) AS october,
    		MAX(CASE WHEN (`month` = 11) THEN growth ELSE NULL END) AS november,
    		MAX(CASE WHEN (`month` = 12) THEN growth ELSE NULL END) AS december
    	FROM calculate_growth
    	GROUP BY stockName, `year`, priceValue
    ),
    calculate_total as (
		select 
			stockName,
			year,
            avg(growth) as total
		from calculate_growth
        group by year, stockName, priceValue
    )
SELECT set_month_growth.*, calculate_total.total
FROM set_month_growth
		left join
		calculate_total ON calculate_total.year = set_month_growth.year;