INSERT INTO stockmarketdestination.average_months_C_С (stockName, priceValue, avg_jan, avg_feb, avg_mar, avg_apr, avg_may, avg_june, avg_july, avg_aug, avg_sep, avg_oct, avg_nov, avg_dec, avg_total)
select
	stockName,
	priceValue,
    avg(january) avg_jan,
    avg(february) avg_feb,
    avg(march) avg_mar,
    avg(april) avg_apr,
    avg(may) avg_may,
    avg(june) avg_june,
    avg(july) avg_july,
    avg(august) avg_aug,
    avg(september) avg_sep,
    avg(october) avg_oct,
    avg(november) avg_nov,
    avg(december) avg_dec,
    avg(total) avg_total
from stockmarketdestination.table_c_с