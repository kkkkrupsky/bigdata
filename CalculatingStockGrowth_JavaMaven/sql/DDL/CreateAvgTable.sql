USE StockMarket;
DROP TABLE IF EXISTS AvgMonth;
CREATE TABLE IF NOT EXISTS AvgMonth(
    stockName VARCHAR(50) NOT NULL,
    avgJanuary DECIMAL(10, 4),
    avgFebruary DECIMAL(10, 4),
    avgMarch DECIMAL(10, 4),
    avgApril DECIMAL(10, 4),
    avgMay DECIMAL(10, 4),
    avgJune DECIMAL(10, 4),
    avgJuly DECIMAL(10, 4),
    avgAugust DECIMAL(10, 4),
    avgSeptember DECIMAL(10, 4),
    avgOctober DECIMAL(10, 4),
    avgNovember DECIMAL(10, 4),
    avgDecember DECIMAL(10, 4),
    avgTotal DECIMAL(10, 4)
);