INSERT INTO AvgMonth
SELECT stockName,
       AVG(january),
       AVG(february),
       AVG(march),
       AVG(april),
       AVG(may),
       AVG(june),
       AVG(july),
       AVG(august),
       AVG(september),
       AVG(october),
       AVG(november),
       AVG(december),
       AVG(total)
FROM stockmarket.stockgrowth
GROUP BY stockName;