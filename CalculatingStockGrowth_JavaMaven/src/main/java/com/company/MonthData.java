package com.company;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MonthData {

    private final Date startMonth;
    private final Date endMonth;
    private final double ClosePrice;
    private final double OpenPrice;


    public MonthData(String startMonth, String endMonth, String Close, String Open) throws ParseException {
        this.startMonth =  new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(startMonth);
        this.endMonth =  new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(endMonth);
        this.ClosePrice = Double.parseDouble(Close.replace(',', '.'));
        this.OpenPrice = Double.parseDouble(Open.replace(',', '.'));
    }

    public double getClosePrice() {
        return this.ClosePrice;
    }

    public double getOpenPrice() {
        return this.OpenPrice;
    }

}































