package com.company;

import com.company.Utils.AverageCalculator;
import com.company.Utils.ManagerDB;

import org.apache.commons.cli.*;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class Main {

    public static List<Year> getDataFromOpenFiles(String pathToDataDir, String delimiter, String type) throws IOException, ParseException {
        List<Year> ListYearData = new ArrayList<>();
        String[] files = Arrays.stream(
                        Objects.requireNonNull(new File(pathToDataDir).listFiles()))
                .map(File::toString)
                .filter((str) -> str.endsWith(".csv")).toArray(String[]::new);

        for (String file : files) {
            Year yearMetrics = Year.getDataFromCsv(file, delimiter);
            yearMetrics.CalculateGrowthMonth(type);
            ListYearData.add(yearMetrics);
        }
        return ListYearData;
    }

    public static void main(String[] args) throws IOException, ParseException, SQLException {
        String[] arguments = getParsedArguments(args);

        String pathToDataDir = arguments[0];
        String type = arguments[1];
        String delimiter = arguments[2];

        List<Year> ListYearData = getDataFromOpenFiles(pathToDataDir, delimiter, type);

        double[] AverageMonths = AverageCalculator.getAverageMonths(ListYearData);
        double AverageTotal = AverageCalculator.getAverageTotal(ListYearData);

        Connection connection = ManagerDB.getConnection();

        ManagerDB.InsertDataToDB(ListYearData, AverageMonths, AverageTotal, connection);
    }

    private static String[] getParsedArguments(String[] args) {
        Options options = new Options();

        Option dataDirOption = new Option("dd", "data-dir", true, "Csv Files");
        dataDirOption.setRequired(true);
        options.addOption(dataDirOption);

        Option processingMethodOption = new Option("pm", "processing-method", true, " ");
        processingMethodOption.setRequired(true);
        options.addOption(processingMethodOption);

        Option delimiterOption = new Option("dt", "delimiter", true, " ");
        delimiterOption.setRequired(true);
        options.addOption(delimiterOption);

        CommandLineParser parser = new DefaultParser();
        CommandLine commandLine = null;
        try {
            commandLine = parser.parse(options, args);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            System.exit(1);
        }

        String dataDir = commandLine.getOptionValue("data-dir");
        String processingMethod = commandLine.getOptionValue("processing-method");
        String delimiter = commandLine.getOptionValue("delimiter");

        return new String[]{dataDir, processingMethod, delimiter};
    }
}
