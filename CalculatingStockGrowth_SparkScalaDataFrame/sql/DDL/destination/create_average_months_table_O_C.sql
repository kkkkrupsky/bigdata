USE stockmarketdestination;

DROP TABLE IF EXISTS average_months_O_C;
CREATE TABLE average_months_O_C(
       StockName VARCHAR(25) NOT NULL,
       PriceValue VARCHAR(3),
       `avg(jan)` DECIMAL(10, 4),
       `avg(feb)` DECIMAL(10, 4),
       `avg(mar)` DECIMAL(10, 4),
       `avg(apr)` DECIMAL(10, 4),
       `avg(may)` DECIMAL(10, 4),
       `avg(june)` DECIMAL(10, 4),
       `avg(july)` DECIMAL(10, 4),
       `avg(aug)` DECIMAL(10, 4),
       `avg(sep)` DECIMAL(10, 4),
       `avg(oct)` DECIMAL(10, 4),
       `avg(nov)` DECIMAL(10, 4),
       `avg(dec)` DECIMAL(10, 4),
       `avg(total)` DECIMAL(10, 4)
);

