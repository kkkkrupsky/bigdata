USE StockMarketDestination;

DROP TABLE IF EXISTS Table_O_C;
CREATE TABLE Table_O_C (
    StockName VARCHAR(25) NOT NULL,
    PriceValue VARCHAR(3),
    year SMALLINT NOT NULL,
    jan DECIMAL(10, 4),
    feb DECIMAL(10, 4),
    mar DECIMAL(10, 4),
    apr DECIMAL(10, 4),
    may DECIMAL(10, 4),
    june DECIMAL(10, 4),
    july DECIMAL(10, 4),
    aug DECIMAL(10, 4),
    sep DECIMAL(10, 4),
    oct DECIMAL(10, 4),
    nov DECIMAL(10, 4),
    `dec` DECIMAL(10, 4),
    total DECIMAL(10, 4)
);
