package com.company
import org.apache.spark.sql.{DataFrame, SaveMode, SparkSession}
import org.apache.spark.sql.functions._
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.types._
import com.typesafe.config.ConfigFactory
import java.io.File
import scala.io.Source


object App {

  def getDataFromFiles(pathToDataDir: String, delimiter: String, processingMethod: String): Array[DataFrame] = {
    val sparkSession = getSparkSession
    val files = new File(pathToDataDir)
      .listFiles.map(_.toString)
      .filter(_.endsWith(".csv"))

    for (file <- files) yield ReadFiles(file, delimiter, sparkSession, processingMethod)
  }

  def getType(raw: String): DataType = {
    raw match {
      case "ByteType" => ByteType
      case "ShortType" => ShortType
      case "IntegerType" => IntegerType
      case "LongType" => LongType
      case "FloatType" => FloatType
      case "DoubleType" => DoubleType
      case "BooleanType" => BooleanType
      case "TimestampType" => TimestampType
      case _ => StringType
    }
  }

  def getChangedSchema(dataFrame: DataFrame):DataFrame = {
    val src = Source.fromFile(getClass.getClassLoader.getResource("schema.txt").getPath)
    val columns: String = src.getLines.find(_ => true).get
    src.close

    val schema = columns
      .split(",")
      .map(_.split(" "))
      .map(x => StructField(x(0), getType(x(1)), true))

    val s1 = dataFrame.schema.fields.map(f => (f.name, f.nullable))
    val s2 = schema.map( f=> (f.name, f.nullable))
    val missingFields = s2.diff(s1)

    var temp: DataFrame = dataFrame

    for (field <- missingFields){
      temp = temp.withColumn(field._1, lit(null: DoubleType))
    }
   temp
  }
  def getSelectedAndRenamedDF(dataFrame: DataFrame):DataFrame = {
    dataFrame.
      select(
        col("StockName"),
        col("PriceValue"),
        col("year"),
        col("1").as("jan"),
        col("2").as("feb"),
        col("3").as("mar"),
        col("4").as("apr"),
        col("5").as("may"),
        col("6").as("june"),
        col("7").as("july"),
        col("8").as("aug"),
        col("9").as("sep"),
        col("10").as("oct"),
        col("11").as("nov"),
        col("12").as("dec"),
        col("avg(growth)").as("total")
      )
  }
  def main(args: Array[String]): Unit = {

    val (pathToDataDir, processingMethod, delimiter) = getArguments(args)

    val yearMetricsArray = getDataFromFiles(pathToDataDir, delimiter, processingMethod)
    val GrowthAllYearsArray = getCalculateGrowth(yearMetricsArray)

    val GrowthAllYears = GrowthAllYearsArray
      .map(growthYear => getChangedSchema(growthYear))
      .reduce(_ union _) //union array of df

    val SelectedColumnsStatistics = getSelectedAndRenamedDF(GrowthAllYears)
    val AveragesStatistics = getAverageMonths(SelectedColumnsStatistics)

    InsertDataIntoDB(processingMethod, SelectedColumnsStatistics, AveragesStatistics)

  }

  def InsertDataIntoDB(processingMethod: String, SelectedColumnsStatistics: DataFrame, AveragesStatistics: DataFrame):Unit = {
    val conf = ConfigFactory.load()
    val url = conf.getString("db.url")
    val user = conf.getString("db.user")
    val password = conf.getString("password")
    val driver = conf.getString("db.driver")

    val properties = new java.util.Properties()
    properties.put("user", user)
    properties.put("password", password)
    properties.put("driver", driver)
    Class.forName("com.mysql.jdbc.Driver").newInstance


    if (processingMethod == "o-c") {
      SelectedColumnsStatistics.write.mode(SaveMode.Overwrite).jdbc(url,"table_O_C",properties)
      AveragesStatistics.write.mode(SaveMode.Overwrite).jdbc(url,"average_months_O_C",properties )
    } else {
      SelectedColumnsStatistics.write.mode(SaveMode.Overwrite).jdbc(url,"table_c_c",properties)
      AveragesStatistics.write.mode(SaveMode.Overwrite).jdbc(url,"average_months_c_с",properties )
    }
  }

  def getAverageMonths(GrowthAllYears: DataFrame): DataFrame = {
      GrowthAllYears
        .select(
          last("StockName").as("StockName"),
          last("PriceValue").as("PriceValue"),
          avg("jan"),
          avg("feb"),
          avg("mar"),
          avg("apr"),
          avg("may"),
          avg("june"),
          avg("july"),
          avg("aug"),
          avg("sep"),
          avg("oct"),
          avg("nov"),
          avg("dec"),
          avg("total")
        ).drop("avg(year)")
  }

  def CalculateGrowthPerYear(yearMetrics: DataFrame) : DataFrame = {
    yearMetrics
      .withColumn("growth", (col("close") - col("open"))/col("open") * 100)
      .select(
        last("StockName").as("StockName"),
        last("PriceValue").as("PriceValue"),
        last("year").as("year"),
        avg("growth")
      ).as("t1")
      .join(
        yearMetrics
          .withColumn("growth", (col("close") - col("open"))/col("open") * 100)
          .select(
            col("year").as("yearTemp"),
            col("month"),
            col("growth")
          )
          .groupBy("yearTemp").pivot("month").agg(first("growth")
      ).as("t2"), col("t1.year") === col("t2.yearTemp"),"inner")
  }
  def getCalculateGrowth(yearMetricsArray: Array[DataFrame]): Array[DataFrame] = {
    for {
      yearMetrics <- yearMetricsArray
      growthPerYear = CalculateGrowthPerYear(yearMetrics)
    } yield growthPerYear
  }

  def getSparkSession: SparkSession ={
    val spark = org.apache.spark.sql.SparkSession.builder
      .master("local")
      .appName("App")
      .getOrCreate
    spark
  }
  def ReadFiles(pathToFile: String, delimiter: String, spark: SparkSession, processingMethod: String): DataFrame = {
    val StockName = pathToFile.split("_")(0).substring(pathToFile.lastIndexOf("""\""") + 1)
    val selectPriceValue= raw"(?<=Secs_)(\w{3})"r
    val PriceValue = selectPriceValue.findFirstIn(pathToFile).mkString

    val df = spark.read
      .format("csv")
      .option("delimiter", delimiter)
      .option("header", "true") //first line in file has headers
      .load(pathToFile)

    val extractedMonths = df.withColumn("month", month(col("Time (UTC)")))
      .withColumn("year", year(col("Time (UTC)"))) //RIGHT!!!!!

    val windowForPreviousRow = Window.orderBy("month")
    val windowForFirst = Window.partitionBy("month").orderBy(col("month"))
    val windowForLast = Window.partitionBy("month")

    val carvedRows = extractedMonths
      .withColumn("row_number", row_number.over(windowForFirst))
      .withColumn("lastRow", last(col("row_number")).over(windowForLast))
      .filter("row_number = lastRow OR row_number == 1")

    var MonthMetrics = carvedRows
      .withColumn("open", regexp_replace(col("Open"), ",", ".").cast("float"))
      .withColumn("close", regexp_replace(col("Close"), ",", ".").cast("float"))
      .select(
        col("year"),
        col("month"),
        col("open"),
        col("close")
      )

    if (processingMethod == "c-c") {
      MonthMetrics = MonthMetrics
        .withColumn("row_number", row_number.over(windowForFirst))
        .withColumn(
          "PrevClosePrice",
          coalesce(
            lag("close", 1).over(windowForPreviousRow),
            col("open")
          )
        ).select(
        col("year"),
        col("month"),
        col("PrevClosePrice").as("open"),
        col("close")
      )
    }
    val CleanMonthMetrics = MonthMetrics
      .withColumn("row_number", row_number.over(windowForFirst))
      .select(
        col("year"),
        col("month"),
        col("open"),
        col("row_number")
      ).as("t1")
      .join(MonthMetrics
        .withColumn("row_number", row_number.over(windowForFirst))
        .select(
          col("month"),
          col("close"),
          col("row_number")
        ).as("t2"),
        col("t1.month") === col("t2.month") && col("t1.row_number") + 1 === col("t2.row_number"),
        "inner")
      .withColumn("StockName", lit(StockName: String))
      .withColumn("PriceValue",lit(PriceValue: String))
      .select(
        col("StockName"),
        col("PriceValue"),
        col("year"),
        col("t1.month").as("month"),
        col("open"),
        col("close")
      ).orderBy("year","month")

    CleanMonthMetrics
  }

  def getArguments(args: Array[String]): (String, String, String) = {
    val help: String = """
                         |Using:
                         |-dd, --data-dir <pathToDataDir>
                         |-pm, --processingMethod <processingMethod>
                         |-dt, --delimiter <delimiter>
                         |""".stripMargin

    if (args.length < 6) {
      throw new IllegalArgumentException(help)
    } else {

      var pathToDataDir = ""
      var processingMethod = ""
      var delimiter = ""

      args.sliding(2, 2).toList.collect {
        case Array(option: String, argPath: String) if option == "-dd" || option == "--data-dir" =>  pathToDataDir = argPath
        case Array(option: String, argMethod: String) if option == "-pm" || option == "--processingMethod" => processingMethod = argMethod
        case Array(option: String, argDelimiter: String) if option == "-dt" || option == "--delimiter" => delimiter = argDelimiter
        case _ => throw new IllegalArgumentException("Unknown option\n" + help)
      }
      (pathToDataDir, processingMethod, delimiter)
    }
  }
}
