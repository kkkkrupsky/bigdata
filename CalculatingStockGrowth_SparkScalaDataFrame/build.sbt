name := "CalculatingStockGrowth_SparkScalaDataFrame"

version := "0.1"


idePackagePrefix := Some("com.company")

scalaVersion := "2.10.4"

libraryDependencies ++= Seq(
  "org.apache.spark" %% "spark-sql" % "2.1.1",
 "mysql" % "mysql-connector-java" % "8.0.26",
  "com.typesafe" % "config" % "1.4.1"
)



