//import com.typesafe.config.ConfigFactory
import com.typesafe.config.ConfigFactory
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.functions.{avg, last}
import org.apache.spark.sql.{DataFrame, SaveMode, SparkSession}
import org.apache.spark.util.CollectionAccumulator

import java.io.File
import java.text.SimpleDateFormat
import java.util.Calendar



object App {

  var PreviousRow: Array[String] = null

  def getFilesInDir(pathToDataDir: String): Array[String] = {
    val files = new File(pathToDataDir)
      .listFiles.map(_.toString)
      .filter(_.endsWith(".csv"))

    files
  }

  def getSparkSession: SparkSession ={
    val spark = org.apache.spark.sql.SparkSession.builder
      .master("local")
      .appName("App")
      .getOrCreate

    spark
  }

  def getTransformRDD(yearMetrics: RDD[Array[String]]):RDD[((Int,Int),(Double,Double))] = {
    val ExtractedMonthYear = yearMetrics.map(row => {
      val DateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(row(0))
      val cal = Calendar.getInstance
      cal.setTime(DateFormat)
      ((cal.get(Calendar.YEAR), cal.get(Calendar.MONTH) + 1), (DateFormat, row(1),row(4)))
    })
    val rddStartMonth = ExtractedMonthYear.reduceByKey((x, y) => {
      if (x._1.before(y._1)) x else y
    })
      .map(row=> {
        ((row._1._1, row._1._2), row._2._2.toDouble)
      })

    val rddEndMonth = ExtractedMonthYear.reduceByKey((x, y) => {
      if (x._1.after(y._1))  x else y
    })
      .map(row=> {
        ((row._1._1, row._1._2), row._2._3.toDouble)
      })

    rddStartMonth.join(rddEndMonth).sortByKey()
  }

  def ReadFiles(pathToFile: String, delimiter: String): RDD[Array[String]] = {
    val spark = getSparkSession
    val indexesAcc: CollectionAccumulator[Long] = spark.sparkContext.collectionAccumulator[Long]("Indexes")

    def FindStartEnd(row: Array[String], index: Long):Unit ={
      if (PreviousRow == null) {
        PreviousRow = row
        indexesAcc.add(index)
      } else {
        val month = row(0).slice(5, 7)
        val monthPreviousRow = PreviousRow(0).slice(5, 7)
        if (month != monthPreviousRow) {
          indexesAcc.add(index - 1)
          indexesAcc.add(index)
          PreviousRow = row
        } else {
          PreviousRow = row
        }
      }
    }

    val rddFromFile = spark.sparkContext.textFile(pathToFile)
    val header = rddFromFile.first() //extract header
    val rddWithoutHeader = rddFromFile
      .filter(row => row != header)
      .map(line => {
        line
          .replace(",", ".").split(delimiter)
      })
      .zipWithIndex()

    val rddRowCount = rddWithoutHeader.count()

    rddWithoutHeader
      .foreach{
      case(yearMetrics, index) => FindStartEnd(yearMetrics, index)
    }

    val ArrayIndexes = indexesAcc.value.toArray

    val YearMetrics = rddWithoutHeader
      .filter{
        case(_, index) => ArrayIndexes.contains(index) || rddRowCount - 1  == index
      }

    val cleanYearMetrics = YearMetrics
      .map{
        case (yearMetrics,_) => yearMetrics
      }

    cleanYearMetrics
  }

  def CalculatingGrowthPerYear(yearMetrics: RDD[Array[String]], processingMethod: String,
                               stockName: String, priceValue:String): RDD[(String, String, Int, String,String, String,String, String, String, String
                                                                                            ,String, String, String, String, String, Double)] ={
    def calculator(openPrice: Double, closePrice: Double):Double ={
      (closePrice - openPrice) / openPrice * 100
    }

    val OpenClose = getTransformRDD(yearMetrics)
    var PreviousClose: Double = 0.0

    val CloseClose = OpenClose.map(row => {
      val temp = PreviousClose
      PreviousClose = row._2._2
      if (temp == 0.0){
        ((row._1._1, row._1._2), (row._2._1, row._2._2))
      }else{
        ((row._1._1, row._1._2), (temp, row._2._2))
      }
    })

    val GrowthPerYear = if (processingMethod == "o-c") {
      OpenClose
        .map(row => (row._1._1, (row._1._2, calculator(row._2._1, row._2._2))))
    } else {
      CloseClose
        .map(row => (row._1._1, (row._1._2, calculator(row._2._1, row._2._2))))
    }
    val PivotGrowthPerYear = GrowthPerYear.groupByKey().mapValues(value => value.toList).map(row => {
      var jan, feb, mar, apr, may, june, july, aug, sep, oct, nov, dec: String = null
      var sum = 0.0
      var count = 0
      for (value <- row._2) {
        value._1 match {
          case 1 => jan = value._2.toString
          case 2 => feb = value._2.toString
          case 3 => mar = value._2.toString
          case 4 => apr = value._2.toString
          case 5 => may = value._2.toString
          case 6 => june = value._2.toString
          case 7 => july = value._2.toString
          case 8 => aug = value._2.toString
          case 9 => sep = value._2.toString
          case 10 => oct = value._2.toString
          case 11 => nov = value._2.toString
          case 12 => dec = value._2.toString
        }
        if (value._2.toString != null){
          sum += value._2
          count += 1
        }
      }
      (stockName, priceValue, row._1, jan, feb, mar, apr, may, june, july, aug, sep, oct, nov, dec, sum/count)
    })

    PivotGrowthPerYear
  }

  def ExtractStockNameAndPriceValue(file: String): (String, String) ={
    val StockName = file.split("_")(0).substring(file.lastIndexOf("""\""") + 1)
    val selectPriceValue = raw"(?<=Secs_)(\w{3})"r
    val PriceValue = selectPriceValue.findFirstIn(file).mkString
    (StockName, PriceValue)
  }

  def getAverageMonthsStatistics(yearStatisticsDF: DataFrame):DataFrame ={
    yearStatisticsDF
      .select(
      last("StockName").as("StockName"),
      last("PriceValue").as("PriceValue"),
      avg("jan"),
      avg("feb"),
      avg("mar"),
      avg("apr"),
      avg("may"),
      avg("june"),
      avg("july"),
      avg("aug"),
      avg("sep"),
      avg("oct"),
      avg("nov"),
      avg("dec"),
      avg("total")
    ).drop("avg(year)")
  }

  def main(args: Array[String]): Unit = {

    val (pathToDataDir, processingMethod, delimiter) = getArguments(args)
    val files = getFilesInDir(pathToDataDir)

    val growthAllYearsArray = for {
      file <- files
      (stockName, priceValue) = ExtractStockNameAndPriceValue(file)
      yearMetrics = ReadFiles(file,delimiter)
    } yield CalculatingGrowthPerYear(yearMetrics, processingMethod, stockName, priceValue)
    val growthMergedArrays = growthAllYearsArray.reduce(_ union _)


    val spark = getSparkSession


    val yearStatisticsDF = spark.createDataFrame(growthMergedArrays).toDF("StockName", "PriceValue", "year", "jan", "feb","mar", "apr", "may","june","july","aug","sep","oct","nov","dec","total")
    val AverageMonthsStatisticsDF = getAverageMonthsStatistics(yearStatisticsDF)
    InsertDataIntoDB(processingMethod,yearStatisticsDF, AverageMonthsStatisticsDF)

  }
  def InsertDataIntoDB(processingMethod: String, yearStatistics: DataFrame, AverageMonthsStatistics: DataFrame):Unit = {
    val conf = ConfigFactory.load()
    val url = conf.getString("db.url")
    val user = conf.getString("db.user")
    val password = conf.getString("db.password")
    val driver = conf.getString("db.driver")

    val properties = new java.util.Properties()
    properties.put("user", user)
    properties.put("password", password)
    properties.put("driver", driver)
    Class.forName("com.mysql.jdbc.Driver").newInstance


    if (processingMethod == "o-c") {
      yearStatistics.write.mode(SaveMode.Overwrite).jdbc(url,"table_O_C",properties)
      AverageMonthsStatistics.write.mode(SaveMode.Overwrite).jdbc(url,"average_months_O_C",properties )
    } else {
      yearStatistics.write.mode(SaveMode.Overwrite).jdbc(url,"table_C_C",properties)
      AverageMonthsStatistics.write.mode(SaveMode.Overwrite).jdbc(url,"average_months_c_с",properties )
    }
  }
  def getArguments(args: Array[String]): (String, String, String) = {
    val help: String = """
                         |Using:
                         |-dd, --data-dir <pathToDataDir>
                         |-pm, --processingMethod <processingMethod>
                         |-dt, --delimiter <delimiter>
                         |""".stripMargin

    if (args.length < 6) {
      throw new IllegalArgumentException(help)
    } else {

      var pathToDataDir = ""
      var processingMethod = ""
      var delimiter = ""

      args.sliding(2, 2).toList.collect {
        case Array(option: String, argPath: String) if option == "-dd" || option == "--data-dir" =>  pathToDataDir = argPath
        case Array(option: String, argMethod: String) if option == "-pm" || option == "--processingMethod" => processingMethod = argMethod
        case Array(option: String, argDelimiter: String) if option == "-dt" || option == "--delimiter" => delimiter = argDelimiter
        case _ => throw new IllegalArgumentException("Unknown option\n" + help)
      }
      (pathToDataDir, processingMethod, delimiter)
    }
  }

}