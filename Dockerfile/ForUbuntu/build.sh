#!/bin/bash

MySQLUser=$(sed -n 1p config.txt)
MySQLPassword=$(sed -n 2p config.txt)

docker build -t test . --build-arg MySQLUser=$MySQLUser --build-arg MySQLPassword=$MySQLPassword 