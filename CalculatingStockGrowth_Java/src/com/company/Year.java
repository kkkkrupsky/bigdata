package com.company;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

public class Year {
    final private int numberOfYear;
    final private List<MonthData> monthData;
    final private String StockName;
    private List<Double> Growth;

    public Year(int yearNumber ,List<MonthData> monthData, String StockName) {
        this.numberOfYear = yearNumber;
        this.monthData = monthData;
        this.StockName = StockName;

    }
    public List<Double> getGrowth(){
        return this.Growth;
    }

    public void setGrowth(List<Double> Growth){
        this.Growth = Growth;
    }

    public static Year getDataFromCsv(String pathToDataFile, String delimiter) throws IOException, ParseException {
        BufferedReader br = new BufferedReader(new FileReader(pathToDataFile));
        List<MonthData> DataList = new ArrayList<>();

        String line;
        String startMonth, endMonth = null, OpenPrice, ClosePrice = null;

        int month = 1;


        br.readLine();
        String[] itemRow = br.readLine().split(delimiter);
        startMonth = itemRow[0];
        OpenPrice = itemRow[1];
        int currentYear =  Integer.parseInt(itemRow[0].substring(0, 4));

        while ((line = br.readLine()) != null) {
            itemRow = line.split(delimiter);

            String date = itemRow[0];
            int monthRow = Integer.parseInt(date.substring(5, 7));

            if (month == monthRow) {
                endMonth = date;
                ClosePrice = itemRow[4];
            }
            else{
                month++;
                MonthData monthData = new MonthData(startMonth, endMonth, ClosePrice, OpenPrice);
                DataList.add(monthData);

                startMonth = date;
                OpenPrice = itemRow[1];
            }
        }
        MonthData monthData = new MonthData(startMonth, endMonth, ClosePrice, OpenPrice);
        DataList.add(monthData);

        String StockName = new File(pathToDataFile).getName().split("_")[0];

        br.close();
        return new Year(currentYear ,DataList, StockName);
    }

    public void CalculateGrowthMonth(String type){
        List<MonthData> DataList = this.getMonthData();
        List<Double> monthGrowth = new ArrayList<>();

        double ClosePrice,  OpenPrice,  growth ;

        switch (type){
            case "o-c":
                for (MonthData Month : DataList){
                    growth = (Month.getClosePrice() - Month.getOpenPrice()) / Month.getOpenPrice() * 100;
                    monthGrowth.add(growth);
                }
                break;
            case "c-c":
                for (int i = 0; i < DataList.size();i++){
                    if (i == 0){
                        OpenPrice = DataList.get(i).getOpenPrice();
                    }else{
                        OpenPrice = DataList.get(i-1).getClosePrice();
                    }
                    ClosePrice = DataList.get(i).getClosePrice();
                    growth = (ClosePrice -OpenPrice) / OpenPrice * 100;
                    monthGrowth.add(growth);
                }

                break;
            case "o-o":
                for (int i = 0; i < DataList.size();i++){
                    if ( i < DataList.size() - 1 ){
                        ClosePrice = DataList.get(i+1).getOpenPrice();
                    }else {
                        ClosePrice = DataList.get(i).getClosePrice();
                    }
                    OpenPrice = DataList.get(i).getOpenPrice();
                    growth = (ClosePrice -OpenPrice) / OpenPrice * 100;
                    monthGrowth.add(growth);
                }
                break;
            default:
                System.out.println("Error input");
        }
        this.setGrowth(monthGrowth);
    }
    public double getGrowthTotal(){
        return this.Growth.stream()
                .mapToDouble(d -> d)
                .average()
                .orElse(0.0);
    }

    public String getStockName(){
        return StockName;
    }

    public int getNumberOfYear() {
        return numberOfYear;
    }

    public List<MonthData> getMonthData() {
        return monthData;
    }

}
