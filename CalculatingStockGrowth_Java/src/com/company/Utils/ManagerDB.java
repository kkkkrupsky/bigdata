package com.company.Utils;

import com.company.Year;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.*;
import java.util.List;
import java.util.Properties;

public class ManagerDB {

    public static String getQueryFromFile(String pathToQuery) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(pathToQuery));
        return br.readLine();
    }
    public static void InsertDataToDB(List<Year> ListYearData, double[] AverageMonths, double AverageTotal, Connection connection) throws SQLException, IOException {
        String nameStock = null;
        String sqlQueryInsertGrowth = getQueryFromFile("sql/DML/InsertGrowthYear.sql");
        String sqlQueryInsertAvgGrowth = getQueryFromFile("sql/DML/InsertAverageGrowth.sql");

        for (Year yearData : ListYearData) {

            nameStock = yearData.getStockName();
            int currentYear = yearData.getNumberOfYear();
            double totalYearGrowth = yearData.getGrowthTotal();

            PreparedStatement preparedStatement = connection.prepareStatement(sqlQueryInsertGrowth);
            preparedStatement.setString(1, nameStock);
            preparedStatement.setInt(2, currentYear);

            for (int i = 3; i < 15; i++) {
                try {
                    preparedStatement.setDouble(i, yearData.getGrowth().get(i - 3));
                } catch (IndexOutOfBoundsException e) {
                    preparedStatement.setNull(i, Types.NULL);
                }
            }
            preparedStatement.setDouble(15, totalYearGrowth);
            preparedStatement.executeUpdate();
        }

        PreparedStatement preparedStatementAvg = connection.prepareStatement(sqlQueryInsertAvgGrowth);
        preparedStatementAvg.setString(1, nameStock);
        for (int i = 2; i < 14; i++){
            try {
                preparedStatementAvg.setDouble(i, AverageMonths[i-2]);
            } catch (IndexOutOfBoundsException e) {
                preparedStatementAvg.setNull(i, Types.NULL);
            }
        }
        preparedStatementAvg.setDouble(14, AverageTotal);
        preparedStatementAvg.executeUpdate();
    }

    public static Connection getConnection() throws IOException {
        Connection connection = null;
        Properties props = new Properties();
        try (InputStream in = Files.newInputStream(Paths.get("database.properties"))) {
            props.load(in);
        }
        try {
            String url = props.getProperty("url");
            String username = props.getProperty("username");
            String password = props.getProperty("password");
            connection = DriverManager.getConnection(url, username, password);
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return connection;
    }
}
