package com.company.Utils;
import com.company.Year;
import java.util.List;

public class AverageCalculator {
    public static double[] getAverageMonths(List<Year> ListYearData) {
        double[] sumMonth = new double[12];
        int[] countsMonths = new int[12];
        double[] AverageMonth = new double[12];

        for (Year yearData :ListYearData) {
            List<Double> monthGrowth = yearData.getGrowth();
            for (int i = 0; i < 12; i++) {
                try {
                    sumMonth[i] += monthGrowth.get(i);
                } catch (IndexOutOfBoundsException e) {
                    continue;
                }
                countsMonths[i] += 1;
            }
        }
        for (int i = 0; i < 12; i++) {
            AverageMonth[i] = sumMonth[i] / countsMonths[i];
        }
        return AverageMonth;
    }

    public static double getAverageTotal(List<Year> ListYearData){
        double sum = 0;
        for (Year yearData: ListYearData) {
            sum += yearData.getGrowthTotal();
        }
        return sum / ListYearData.size();
    }
}
