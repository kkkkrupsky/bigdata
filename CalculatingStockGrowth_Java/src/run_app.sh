#!/bin/bash

source ../database.properties

function db_setup() {
  mysql --host="$host" --user="$username" --password="$password" < sql/DDl/CreateDB.sql
  mysql --host="$host" --user="$username" --password="$password" --database "$database"< sql/DDl/CreateTable.sql
  mysql --host="$host" --user="$username" --password="$password" --database "$database"< sql/DDl/CreateAvgTable.sql
}

while [[ -n "$1" ]]; do

  case $1 in
    -db|--dbsetup)
      db_setup
      shift
      ;;

    -dd|--data-dir)
      if [ -n "$2" ] && [ "${2:0:1}" != "-" ]; then
        data_dir=$2
        shift 2
      else
        echo "Miss one argument $1"
        exit 1
      fi
      ;;

    -pm|--processing-method)
      if [ -n "$2" ] && [ "${2:0:1}" != "-" ]; then
        processing_method=$2
        shift 2
      else
        echo "Miss one argument $1"
        exit 1
      fi
      ;;
    -dt|--delimiter)
      if [ -n "$2" ] && [ "${2:0:1}" != "-" ]; then
        delimiter=$2
          shift 2
        else
          echo "Miss one argument $1"
          exit 1
        fi
        ;;

    *)
      echo "Error $1"
      exit 1
  esac
done

if [ -n "$data_dir" ] && [ -n "$processing_method" ] && [ -n "$delimiter" ]; then
  javac -cp ".;lib/mysql-connector-java-8.0.27.jar;lib/commons-cli-1.3.1.jar"  com/company/*.java com/company/Utils/*.java
  jar -cfvm RUN.jar Manifest.txt com/company/*.class com/company/Utils/*.class
  java -jar RUN.jar -dd "$data_dir" -pm "$processing_method" -dt "$delimiter"
else
  echo "Miss one or more arguments $1"
  exit 1
fi





