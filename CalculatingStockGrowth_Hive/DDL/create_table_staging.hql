USE StockMarketStaging;

DROP TABLE IF EXISTS StockMarketStaging.stock_market_staging;
CREATE EXTERNAL TABLE StockMarketStaging.stock_market_staging
(
	time_utc TIMESTAMP,
	open_price STRING,
	high STRING, 
	low STRING,
	close_price STRING,
	volume STRING
)

PARTITIONED BY (
    stock_name STRING,
    price_value SMALLINT,
    current_year SMALLINT
)
row format delimited
fields terminated by ';'
lines terminated by '\n'
TBLPROPERTIES ("skip.header.line.count"="1");
