USE StockMarketStaging;

DROP TABLE IF EXISTS StockMarketStaging.start_end_month;
CREATE EXTERNAL TABLE StockMarketStaging.start_end_month (
    stock_name STRING,
    price_value SMALLINT,
    current_year SMALLINT,
    current_month SMALLINT,
    open_price FLOAT,
    close_price FLOAT
)
row format delimited
fields terminated by ';'
lines terminated by '\n';