USE StockMarketDestination;
DROP TABLE IF EXISTS StockMarketDestination.destination_growth_table;
CREATE EXTERNAL TABLE StockMarketDestination.destination_growth_table
(
    stock_name        STRING,
    price_value       SMALLINT,
    current_year      SMALLINT,
    processing_method STRING,
    jan               DECIMAL(8, 4),
    feb               DECIMAL(8, 4),
    mar               DECIMAL(8, 4),
    apr               DECIMAL(8, 4),
    may               DECIMAL(8, 4),
    june              DECIMAL(8, 4),
    july              DECIMAL(8, 4),
    aug               DECIMAL(8, 4),
    sep               DECIMAL(8, 4),
    oct               DECIMAL(8, 4),
    nov               DECIMAL(8, 4),
    decm             DECIMAL(8, 4),
    total             DECIMAL(8, 4)
)
row format delimited
fields terminated by ';'
lines terminated by '\n';