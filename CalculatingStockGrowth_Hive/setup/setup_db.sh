#!/usr/bin/env bash
hive -f "gs://stock-data-bucket-test//DDL//create_databases.hql"
hive -f "gs://stock-data-bucket-test//DDL//create_table_staging.hql"
hive -f "gs://stock-data-bucket-test/DDL/create_start_end_month_table.hql"
hive -f "gs://stock-data-bucket-test/DDL/create_destination_growth_table.hql"
hive -f "gs://stock-data-bucket-test/DDL/create_destination_growth_avg_table.hql"
##
##
hdfs dfs -ls -C /data > namefiles.txt
while read filename; do
  hive  -hivevar path_to_file="/data/" \
          -hivevar stock_name="USA500IDXUSD_10" \
          -hivevar price_value=0 \
          -hivevar current_year=2012 \
          -f "gs://stock-data-bucket-test//DML//insert_data_to_staging.hql"
done < namefiles.txt

hive -f 'gs://stock-data-bucket-test//DML//extract_start_end_month.hql'

hive -hivevar processing_method="c-o" \
     -f 'gs://stock-data-bucket-test//DML//calculate_growth.hql'

hive -f 'gs://stock-data-bucket-test//DML//calculate_growth_avg.hql'