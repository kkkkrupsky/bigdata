USE StockMarketDestination;

INSERT INTO TABLE StockMarketDestination.destination_growth_avg_table
SELECT stock_name,
       price_value,
       processing_method,
       avg(jan),
       avg(feb),
       avg(mar),
       avg(apr),
       avg(may),
       avg(june),
       avg(july),
       avg(aug),
       avg(sep),
       avg(oct),
       avg(nov),
       avg(decm),
       avg(total)
FROM StockMarketDestination.destination_growth_table
GROUP BY
    stock_name,
    price_value,
    processing_method;