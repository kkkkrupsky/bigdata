USE StockMarketStaging;

INSERT INTO TABLE StockMarketStaging.start_end_month
SELECT temp.stock_name,
       temp.price_value,
       temp.current_year,
       temp.current_month,
       temp.open_price,
       temp.close_price
FROM (SELECT smt.stock_name AS stock_name,
             smt.price_value AS price_value,
             smt.current_year AS current_year,
             month(time_utc) AS current_month,
             cast(regexp_replace(smt.open_price, ',', '.') as FLOAT) as open_price,
             cast(regexp_replace(smt.close_price, ',', '.') as FLOAT) as close_price,
             ROW_NUMBER() OVER (
                 PARTITION BY
                     stock_name,
                     price_value,
                     current_year,
                     month(time_utc)
                 ORDER BY time_utc) AS seqnum_asc,
             ROW_NUMBER() OVER (
                 PARTITION BY
                    stock_name,
                    price_value,
                    current_year,
                    month(time_utc)
                 ORDER BY time_utc DESC) AS seqnum_desc,
             smt.time_utc as time_utc
        FROM StockMarketStaging.stock_market_staging smt
    ) temp
WHERE  1 IN (seqnum_asc, seqnum_desc)
ORDER BY time_utc;