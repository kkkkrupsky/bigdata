USE StockMarketDestination;
SET processing_method;
SET hivevar:processing_method;

WITH cte AS (
    SELECT stock_name,
           price_value,
           current_year,
           current_month,
           open_price,
           close_price,
           ROW_NUMBER() over (PARTITION BY current_month ORDER BY  current_month) as rn
    FROM StockMarketStaging.start_end_month
    ),
     cte_2 AS (
        SELECT
               t1.stock_name,
               t1.price_value,
               t1.current_year,
               t1.current_month,
               t1.open_price,
               t2.close_price
        FROM  cte t1
            JOIN  cte t2
            ON (t1.current_month = t2.current_month AND t1.rn + 1 = t2.rn)
    ),
     calculating_growth AS (
         SELECT
                stock_name,
                price_value,
                current_year,
                current_month,
                CASE
                    WHEN '${hivevar:processing_method}' = "o-c" THEN
                            (close_price - open_price)
                            / open_price

                    WHEN '${hivevar:processing_method}' = "c-o" THEN
                            (close_price - COALESCE(lag(close_price) OVER w, open_price))
                            / COALESCE(lag(close_price) OVER w, open_price)
                    ELSE NULL
                    END * 100 AS growth
         FROM cte_2
             WINDOW w AS (
                 PARTITION BY
                     stock_name,
                     price_value
                 ORDER BY
                     current_year,
                     current_month
                 )
         ORDER BY current_month
     ),
     calculating_total AS (
         SELECT avg(growth) as total,
                current_year
         FROM calculating_growth
         GROUP BY current_year
    ),
    result AS (
        select
            stock_name,
            current_year,
            price_value,
            '${hivevar:processing_method}',
            sum(case when current_month = 1 then growth else null end),
            sum(case when current_month = 2 then growth else null end),
            sum(case when current_month = 3 then growth else null end),
            sum(case when current_month = 4 then growth else null end),
            sum(case when current_month = 5 then growth else null end),
            sum(case when current_month = 6 then growth else null end),
            sum(case when current_month = 7 then growth else null end),
            sum(case when current_month = 8 then growth else null end),
            sum(case when current_month = 9 then growth else null end),
            sum(case when current_month = 10 then growth else null end),
            sum(case when current_month = 11 then growth else null end),
            sum(case when current_month = 12 then growth else null end)
        FROM calculating_growth
        GROUP BY
            stock_name,
            current_year,
            price_value
    )

INSERT INTO TABLE StockMarketDestination.destination_growth_table
SELECT r.*, c.total
FROM result r
lEFT JOIN calculating_total c
ON c.current_year = r.current_year;

