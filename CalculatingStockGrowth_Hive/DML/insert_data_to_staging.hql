USE StockMarketStaging;

SET path_to_file;
SET hivevar:path_to_file;
SET stock_name;
SET hivevar:stock_name;
SET current_year;
SET hivevar:current_year;
SET price_value;
SET hivevar:price_value;

LOAD DATA INPATH '${hivevar:path_to_file}'
INTO TABLE StockMarketStaging.stock_market_staging
PARTITION(
    stock_name='${hivevar:stock_name}',
    price_value='${hivevar:price_value}',
    current_year='${hivevar:current_year}'
);