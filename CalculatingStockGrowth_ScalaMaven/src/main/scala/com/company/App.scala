package com.company

import com.typesafe.config.ConfigFactory
import scala.collection.mutable.ArrayBuffer
import java.io.File
import java.sql.{Connection, DriverManager, Types}

case class MonthData(StartMonth: String, EndMonth: String, OpenPrice: Float, ClosePrice: Float)
case class YearData(NumberOfYear: Int, StockName: String, months: ArrayBuffer[MonthData])

object App {

  def getDataFromFiles(pathToDataDir: String, delimiter: String): Array[YearData] = {
    val files = new File(pathToDataDir)
      .listFiles.map(_.toString)
      .filter(_.endsWith(".csv"))

    for (file <- files) yield ReadFiles(file, delimiter)
  }

  def ReadFiles(file: String, delimiter: String): YearData = {
    val bufferedSource = io.Source.fromFile(file)

    val FirstLine = bufferedSource
      .getLines.drop(1)
      .next.split(delimiter)

    var StartMonth: String = FirstLine(0)
    var EndMonth: String = ""
    val CurrentYear: Int = FirstLine(0).slice(0, 4).toInt

    var OpenPrice: String = FirstLine(1).replace(',', '.')
    var ClosePrice: String = ""

    var monthOfRow, currentMoth = 1

    val monthData = ArrayBuffer[MonthData]()
    for (line <- bufferedSource.getLines) {
      val cols = line.split(";")
      val Date = cols(0)
      currentMoth = Date.slice(5, 7).toInt

      if (currentMoth == monthOfRow) {
        EndMonth = Date
        ClosePrice = cols(4).replace(',', '.')
      } else {
        monthOfRow = monthOfRow + 1
        monthData += MonthData(StartMonth, EndMonth, OpenPrice.toFloat, ClosePrice.toFloat)
        StartMonth = Date
        OpenPrice = cols(1).replace(',', '.')
      }
    }
    bufferedSource.close

    monthData += MonthData(StartMonth, EndMonth, OpenPrice.toFloat, ClosePrice.toFloat)

    val StockName = file.split("_")(0).substring(file.lastIndexOf("""\""") + 1)
    val yearMetrics: YearData = YearData(CurrentYear, StockName, monthData)

    yearMetrics
  }

  def CalculateGrowthPerYear(yearMetrics: YearData, processingMethod: String): (Int, String, ArrayBuffer[Float], Float)= {
    val yearGrowth = ArrayBuffer[Float]()
    var OpenPrice, ClosePrice, growth: Float = 0

    processingMethod match {
      case "o-c" =>
        for (month <- yearMetrics.months){
          OpenPrice = month.OpenPrice
          ClosePrice = month.ClosePrice
          growth = (ClosePrice -OpenPrice) / OpenPrice * 100
          yearGrowth +=  growth
        }
      case "c-o" =>
        for ( i <- 0 to yearMetrics.months.length) {
          if (i == 0){
            OpenPrice = yearMetrics.months(i).OpenPrice
          } else {
            OpenPrice = yearMetrics.months(i - 1).ClosePrice
          }
          ClosePrice = yearMetrics.months(i).ClosePrice
          growth = (ClosePrice -OpenPrice) / OpenPrice * 100
          yearGrowth +=  growth
        }
    }

    val total: Float = yearGrowth.sum / yearGrowth.length

    (yearMetrics.NumberOfYear, yearMetrics.StockName, yearGrowth, total)
  }

  def getGrowthAllYears(yearMetricsList: Array[YearData], processingMethod: String): Array[(Int, String, ArrayBuffer[Float], Float)] = {
    for {
      yearMetrics <- yearMetricsList
      growthPerYer = CalculateGrowthPerYear(yearMetrics, processingMethod)
    } yield growthPerYer
  }

  def getAverageMonths(yearsData: Array[(Int, String, ArrayBuffer[Float], Float)]): ArrayBuffer[Float] = {
    val sumMonth = new Array[Float](12)
    val countsMonths = new Array[Int](12)
    val Average = new ArrayBuffer[Float]()

    for (year <- yearsData) {
      val months = year._3
      for (i <- 0 to 11) {
        try {
          sumMonth(i) += months(i)
          countsMonths(i) += 1
        } catch {
          case _: IndexOutOfBoundsException => println("")
        }
      }
    }
    sumMonth.map(_ / yearsData.length)
    for (j <- countsMonths.indices) {
      Average += (sumMonth(j)/countsMonths(j))
    }
    Average
  }

  def getAverageTotal(yearsData: Array[(Int, String, ArrayBuffer[Float], Float)]): Float = {
    yearsData.map(_._4).sum / yearsData.length
  }

  def getQueryFromFile(pathToSqlQuery: String): String = {
    val source = io.Source.fromFile(pathToSqlQuery)
    val sqlQuery = try source.mkString finally source.close()
    sqlQuery
  }

  def insertDataIntoDB(connection: Connection, GrowthAllYears: Array[(Int, String, ArrayBuffer[Float], Float)],
                       AverageMonths: ArrayBuffer[Float], AverageTotal: Float): Unit = {

    val conf = ConfigFactory.load()
    val sqlQueryInsertGrowth = getQueryFromFile(conf.getString("pathsToQueries.pathInsertGrowthYear"))
    val sqlQueryInsertAvgGrowth = getQueryFromFile(conf.getString("pathsToQueries.pathInsertAverageGrowth"))

    val preparedStatementGrowth = connection.prepareStatement(sqlQueryInsertGrowth)
    val preparedStatementAvgGrowth = connection.prepareStatement(sqlQueryInsertAvgGrowth)

    for (growthPerYear <- GrowthAllYears){
      preparedStatementGrowth.setString(1, growthPerYear._2)
      preparedStatementGrowth.setInt(2, growthPerYear._1)
      for (i <- 3 to 14){
        try {
          preparedStatementGrowth.setFloat(i, growthPerYear._3(i - 3))
        } catch {
          case _: IndexOutOfBoundsException => preparedStatementGrowth.setNull(i, Types.NULL);
        }
      }
      preparedStatementGrowth.setFloat(15, growthPerYear._4)
      preparedStatementGrowth.executeUpdate()
    }

    preparedStatementAvgGrowth.setString(1, GrowthAllYears(0)._2)
    for (i <- 2 to 13){
      try {
        preparedStatementAvgGrowth.setFloat(i, AverageMonths(i - 2))
      } catch {
        case _ : IndexOutOfBoundsException => preparedStatementGrowth.setNull(i, Types.NULL);
      }
    }
    preparedStatementAvgGrowth.setFloat(14, AverageTotal)
    preparedStatementAvgGrowth.executeUpdate()
  }

  def getArguments(args: Array[String]): (String, String, String)= {
    val help: String = """
        |Using:
        |-dd, --data-dir <pathToDataDir>
        |-pm, --processingMethod <processingMethod>
        |-dt, --delimiter <delimiter>
        |""".stripMargin

    if (args.length < 6) {
      throw new IllegalArgumentException(help)
    } else {

      var pathToDataDir = ""
      var processingMethod = ""
      var delimiter = ""

      args.sliding(2, 2).toList.collect {
        case Array(option: String, argPath: String) if option == "-dd" || option == "--data-dir" =>  pathToDataDir = argPath
        case Array(option: String, argMethod: String) if option == "-pm" || option == "--processingMethod" => processingMethod = argMethod
        case Array(option: String, argDelimiter: String) if option == "-dt" || option == "--delimiter" => delimiter = argDelimiter
        case _ => throw new IllegalArgumentException("Unknown option")
      }
      (pathToDataDir, processingMethod, delimiter)
    }
  }

  def main(args: Array[String]): Unit = {

    val (pathToDataDir, processingMethod, delimiter) = getArguments(args)

    val yearMetricsList: Array[YearData] = getDataFromFiles(pathToDataDir, delimiter)

    val GrowthAllYears = getGrowthAllYears(yearMetricsList, processingMethod)
    val AverageMonths = getAverageMonths(GrowthAllYears)
    val AverageTotal = getAverageTotal(GrowthAllYears)


    val connection = getConnection
    insertDataIntoDB(connection, GrowthAllYears, AverageMonths, AverageTotal)
    connection.close()
  }

  def getConnection: Connection = {
    val conf = ConfigFactory.load()
    val url = conf.getString("db.url")
    val username = conf.getString("db.user")
    val password = conf.getString("db.password")
    try {
      Class.forName("com.mysql.cj.jdbc.Driver")
      DriverManager.getConnection(url, username, password)
    } catch {
      case e: Exception => throw new InterruptedException(s"Error: ${e.getMessage}")
    }
  }
}






