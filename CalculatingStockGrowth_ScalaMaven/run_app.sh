#/bin/bash
source  ./src/main/resources/database.properties

function db_setup() {
  mysql  -u $username -p$password < sql/DDL/CreateDB.sql
  mysql  -u $username -p$password < sql/DDL/CreateTable.sql
  mysql  -u $username -p$password < sql/DDL/CreateAvgTable.sql
}

while [[ -n "$1" ]]; do

  case $1 in
    -db|--dbsetup)
      db_setup
      shift
      ;;

    -dd|--data-dir)
      if [ -n "$2" ] && [ "${2:0:1}" != "-" ]; then
        data_dir=$2
        shift 2
      else
        echo "Miss one argument $1"
        exit 1
      fi
      ;;

    -pm|--processing-method)
      if [ -n "$2" ] && [ "${2:0:1}" != "-" ]; then
        processing_method=$2
        shift 2
      else
        echo "Miss one argument $1"
        exit 1
      fi
      ;;
    -dt|--delimiter)
      if [ -n "$2" ] && [ "${2:0:1}" != "-" ]; then
        delimiter=$2
          shift 2
        else
          echo "Miss one argument $1"
          exit 1
        fi
        ;;

    *)
      echo "Error $1"
      exit 1
  esac
done

if [ -n "$data_dir" ] && [ -n "$processing_method" ] && [ -n "$delimiter" ]; then
  mvn package
    START_TIME=$(date +%s)
  java -jar target/CalculatingStockGrowth_ScalaMaven-1.0-SNAPSHOT-jar-with-dependencies.jar -dd ".\\data\\" -pm "o-c" -dt ";"
    END_TIME=$(date +%s)
    DIFF=$(( $END_TIME - $START_TIME ))
    echo "It took $DIFF seconds"

else
  echo "Miss one or more arguments $1"
  exit 1
fi